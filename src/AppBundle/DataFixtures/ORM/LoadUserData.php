<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Usuario;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData implements ORMFixtureInterface, ContainerAwareInterface {

    private $container;

    public function load(ObjectManager $manager)
    {
        $user = new Usuario();
        $user->setUsername('admin');
        $user->setEmail('admin@gmail.com');

        $enconder = $this->container->get('security.password_encoder');

        $password = $enconder->encodePassword($user, '1234');
        $user->setPassword($password);

        $manager->persist($user);
        $manager->flush();

    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }


}

